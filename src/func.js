const getSum = (str1, str2) => {
  if (typeof str1 === 'string' && typeof str2 === 'string') {
    if (str1.match(/\D+/g) || str2.match(/\D+/g))
      return false;
    let sum = "";
    let str1Length = str1.length;
    let str2Length = str2.length;
    if(str2Length > str1Length ){
      let tem = str2;
      str2 = str1;
      str1 = tem;
    }
    let carry = 0; let a; let b; let temp; let digitSum;
    for (let i = 0; i < str1.length; i++) {
      a = parseInt(str1.charAt(str1.length - 1 - i));
      b = parseInt(str2.charAt(str2.length - 1 - i));
      b = (b) ? b : 0;
      temp = (carry + a + b).toString();
      digitSum = temp.charAt(temp.length - 1);
      carry = parseInt(temp.substr(0, temp.length - 1));
      carry = (carry) ? carry : 0;
      sum = (i === str1.length - 1) ? temp + sum : digitSum + sum;
    }
    return sum;
  }
  return false;
};
const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0;
  let commentCount = 0;
  for (let author in listOfPosts){
    if (listOfPosts[author].author === authorName)
      postCount++;
    for (let comentator in listOfPosts[author].comments) {
      if (listOfPosts[author].comments[comentator].author === authorName)
        commentCount++;
    }
  }
  return `Post:${postCount},comments:${commentCount}`;
};
const tickets=(people)=> {
  let people25 = 0;
  let people50 = 0;
  let people100 = 0;

  for (let money of people) {
    if (Number(money) === 25) {
      people25 += 1;
    }
    else if (Number(money) === 50) {
      if (people25 >= 1){
        people25 -= 1;
        people50 += 1;
      }
      else
        return 'NO';
    }
    else if (Number(money) === 100) {
      if (people25 >= 1 &&  people50 >= 1){
        people25 -= 1;
        people50 -= 1;
        people100 += 1;
      }
      else if (people25 >= 3) {
        people25 -= 3;
        people100 += 1;
      }
      else
        return 'NO';
    }
  }
  return 'YES';
};
module.exports = {getSum, getQuantityPostsByAuthor, tickets};
